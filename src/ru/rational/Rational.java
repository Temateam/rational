package ru.rational;

/**
 * Класс для представления обычной дроби и работы с дробями
 * Работа с дробями: сложение, вычитание, умножение, деление и сокращение дроби
 * @author Мельников Артем, Анохин Владимир
 */
public class Rational {
    private int num;
    private int denum;

    /**
     * Конструктор с параметрами
     * Если знаменатель равен 0, выбрасывает арифметическое исключение
     * @param num числитель
     * @param denum знаменатель
     * @throws ArithmeticException арифметическое исключение
     */
    public Rational(int num, int denum) throws ArithmeticException{
        this.num = num;
        check(denum);
        this.denum = denum;
    }

    /**
     * Конструктор по умолчанию
     * Дробь по умолчанию(1/1)
     */
    public Rational() {
        this.num = 1;
        this.denum = 1;
    }

    /**
     * Конструктор, который позволяет задать только числитель, если знаменатель равен 1
     * @param num числитель
     */
    public Rational(int num){
        this.num = num;
        this.denum = 1;
    }

    /**
     * Конструктор, который позволяет задать дробь в виде строки
     * Строка должна быть в виде: числитель/знаменатель
     * Строка не должна иметь пробелы
     * Если знаменатель равен 0, выбрасывает арифметическое исключение
     * Если на входных данных не дробь, то выбрасывает исключение
     * @param rational дробь(Пример: 1/1)
     * @throws Exception исключение
     */
    public Rational(String rational) throws Exception{
        if(!rational.matches("-?\\d+([/]-?\\d+)?")){
            throw new Exception("Ошибка входных данных");
        }
        if(rational.contains("/")){
            String[] rationalM = rational.split("/");
            this.num = Integer.parseInt(rationalM[0]);
            this.denum = Integer.parseInt(rationalM[1]);
            check(this.denum);
        }else{
            this.num = Integer.parseInt(rational);
            this.denum = 1;
        }
    }

    public int getNum() {
        return num;
    }

    public int getDenum() {
        return denum;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setDenum(int denum) throws ArithmeticException {
        check(denum);
        this.denum = denum;
    }

    private void check(int denum) throws ArithmeticException{
        if (denum == 0) {
            throw new ArithmeticException("Делить на 0 нельзя!");
        }
    }

    /**
     * Метод, который возвращает дробь в виде строки (числитель/знаменатель)
     * Если знаменатель равен 1, вернет только числитель.
     * @return дробь в виде строки
     */
    @Override
    public String toString()
    {
        if(this.denum == 1){
            return num + "";
        }else {
            return num + "/" + denum;
        }
    }

    /**
     * Метод сложения двух дробей.
     * @param rational слагаемое
     */
    public void addition(Rational rational){
        this.num = this.num * rational.denum + this.denum * rational.num;
        this.denum *= rational.denum;
        this.reduction();
    }

    /**
     * Метод деления двух дробей
     * @param rational делитель
     * @throws Exception арифметическое исключение, если числитель делителя равняется 0
     */
    public void division(Rational rational) throws ArithmeticException{
        check(rational.num);
        this.num = this.num * rational.denum;
        this.denum = this.denum * rational.num;
        this.reduction();
    }

    /**
     * Метод вычитания двух дробей
     * @param rational вычитаемое
     */
    public void subtraction(Rational rational) {
        this.num = this.num * rational.denum - this.denum * rational.num;
        this.denum *= rational.denum;
        this.reduction();
    }

    /**
     * Метод умножения двух дробей.
     * @param rational множитель
     */
    public void multiply(Rational rational){
        this.num = this.num * rational.num;
        this.denum = this.denum * rational.denum;
        this.reduction();
    }

    /**
     * Метод сокращения дроби
     */
    public void reduction() {
        if(this.num != 0) {
            if (this.denum < 0) {
                this.denum = -denum;
                this.num = -num;
            }
            int nod;
            int var;
            if (Math.abs(this.num) <= Math.abs(this.denum)) {
                nod = Math.abs(this.denum);
                var = Math.abs(this.num);
            } else {
                nod = Math.abs(this.num);
                var = Math.abs(this.denum);
            }
            while (nod != var) {
                if (nod <= 0) {
                    break;
                } else {
                    if (nod > Math.abs(this.denum)) {
                        nod -= Math.abs(this.denum);
                    } else {
                        nod -= Math.abs(this.num);
                    }
                }
            }
            if (Math.abs(nod) != 1 && this.num % nod == 0) {
                this.num /= nod;
                this.denum /= nod;
            }
        }else{
            this.denum = 1;
        }
    }
}
