package ru.rational;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс для демонстрации работоспособности класса Rational
 * @author Melnikov Artem
 */
public class Demo {
    static int i = 0;
    static ArrayList<String> numbers = new ArrayList();
    static ArrayList<String> symbols = new ArrayList();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String regNumber = "([(][-]\\d+[)]|\\d+)";
        String regRational = regNumber + "([/]" + regNumber + ")?";
        String reg = "^" + regRational + "([+\\-:*]" + regRational + ")+$";
        String exp;

        System.out.println("Введите выражение" + '\n'
                + "Отрицательное число должно быть в круглых скобках, без пробелов между знаками:");
        while (!(exp = scanner.nextLine()).matches(reg)){
            System.out.println("Введенное сообщение не соответствует формату.");
            System.out.println("Введите выражение: ");
        }

        int j = 0;
        for(int i = 0; i < exp.length(); i++){
            String str = exp.substring(i, i+1);
            if(numbers.size() == j){
                numbers.add("");
            }
            if(str.equals("+")|| str.equals("-") || str.equals("*") || str.equals(":")){
                if(str.equals("-") && exp.substring(i-1,i).equals("(")){
                    numbers.set(j, numbers.get(j) + str);
                }else {
                    symbols.add(str);
                    j++;
                }
            }else{
                if(str.equals("(") || str.equals(")")){
                    continue;
                }
                numbers.set(j, numbers.get(j) + str);
            }

        }

        System.out.println("Ответ: " + calculator());
    }


    static Rational calculator(){
        try {
            Rational rational = new Rational(numbers.get(i));


            while ((i += 1) <= symbols.size()) {
                switch (symbols.get(i-1)) {
                    case "+":
                        if (check()) {
                            rational.addition(calculator());
                        } else {
                            rational.addition(new Rational(numbers.get(i)));
                        }
                        break;
                    case "*":
                        if (check()) {
                            rational.multiply(calculator());
                        } else {
                            rational.multiply(new Rational(numbers.get(i)));
                        }
                        break;
                    case ":":
                        if (check()) {
                            rational.division(calculator());
                        } else {
                            rational.division(new Rational(numbers.get(i)));
                        }
                        break;
                    case "-":
                        if (check()) {
                            rational.subtraction(calculator());
                        } else {
                            rational.subtraction(new Rational(numbers.get(i)));
                        }
                        break;

                }
            }
            return rational;
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Rational();
    }

    static boolean check(){
        return i < symbols.size() && (symbols.get(i).equals("*") || symbols.get(i).equals(":"));
    }

}