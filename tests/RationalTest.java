
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Test;
import ru.rational.Rational;

import static org.junit.Assert.*;

/**
 * @author Анохин Владимир, Мельников Артем
 */
public class RationalTest {

    Rational rational1;
    Rational rational2 = new Rational(2, 3);

    @Test
    public void testRational() {
        try {
            Rational rational1 = new Rational(1, 2);
            assertEquals("1/2", rational1.toString());

            rational1 = new Rational(0, 1);
            assertEquals("0", rational1.toString());
            rational1 = new Rational(1, 0);
            assertEquals("0", rational1.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getNum() {
        try {
            assertEquals("1", rational1.getNum());
            assertEquals("2", rational2.getNum());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getDenum() {
        try {
            assertEquals("1", rational1.getDenum());
            assertEquals("0", rational1.getDenum());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddition() {
        try {
            rational1.addition(rational2);
            assertEquals("7/6", rational1.toString());

            rational1 = new Rational(-1, -2);
            rational1.addition(rational2);
            assertEquals("7/6", rational1.toString());

            rational1 = new Rational(0);
            rational1.addition(rational2);
            assertEquals("2/3", rational1.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDivision() {
        try {
            rational1.division(rational2);
            assertEquals("3/4", rational1.toString());


            rational1 = new Rational(0);
            rational1.division(rational2);
            assertEquals("0", rational1.toString());

            rational1 = new Rational(-5, -3);
            rational1.division(rational2);
            assertEquals("15/6", rational1.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSubtraction() {

        try {
            rational1 = new Rational(1, 4);

            rational1.subtraction(rational2);
            assertEquals("-5/12", rational1.toString());

            rational1 = new Rational(-2, -3);
            rational1.subtraction(rational2);
            assertEquals("0", rational1.toString());

            rational1 = new Rational(0);
            rational1.subtraction(rational2);
            assertEquals("-2/3", rational1.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMultiply() {

        try {
            rational1 = new Rational(0, 3);
            rational1.multiply(rational2);
            assertEquals("0", rational1.toString());

            rational1 = new Rational(-5, -4);
            rational1.multiply(rational2);
            assertEquals("5/6", rational1.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
